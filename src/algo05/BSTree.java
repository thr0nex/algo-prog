package algo05;

/**
 * Implementation of Binary Search Tree
 * @author yannickstreicher, benjaminrebholz, silviahimmelseher
 */
public class BSTree {
    private TreeNode root;

    /**
     * Constructor for an empty binary search tree.
     */
    public BSTree() {
    }

    /**
     * Constructor for a binary search tree that contains the given element.
     * 
     * @param value the value to be inserted.
     */
    public BSTree(int value) {
        this.root = new BSTreeNode(value);
    }

    /**
     * Inserts a given value in the binary search tree. (3 Punkte)
     * 
     * @param value the value to be inserted.
     */
    public void insert(int value) {
        // TODO: Implement the algorithm here.
        // this implementation orients the pseudocode from the clrs book
        /* we need this y, to keep track of the parent node */
        TreeNode y = null;
        /* start traversing at the root node */
        TreeNode x = this.root;
        /* a new node for the new element must be created */
        TreeNode z = new BSTreeNode(value);
        while (x != null) {
            y = x;
            /* branch with aid of the search tree property */
            if (z.getValue() < x.getValue()) {
                x = x.getLeftChild();
            } else {
                x = x.getRightChild();
            }
        }
        z.setParentNode(y);
        /* this gets the case if the tree was empty */
        if (y == null) {
            this.root = z;
        } else if (z.getValue() < y.getValue()) {
            y.setLeftChild(z);
        } else {
            y.setRightChild(z);
        }
    }

    // TODO: Implement additional methods here (if needed).

    /**
     * Determines whether or not a given value is within the binary search tree. (2
     * Punkte)
     * 
     * @param value the value to search.
     * @return the tree node containing the search value, if any, or null otherwise.
     */
    public TreeNode search(int value) {
        // TODO: Implement the algorithm here.
        /* the root node is the entry node */
        TreeNode res = this.root;
        return this.search(res, value);
    }

    /**
     * Override of the search method, to scan the BSTree reccursive given a node
     * @param res the current node
     * @param key the searched tree
     * @return the Node if found, else null 
     */
    private TreeNode search(TreeNode res, int key) {
        if (res == null || key == res.getValue()) {
            return res;
        }
        if (key < res.getValue()) {
            return search(res.getLeftChild(), key);
        } else {
            return search(res.getRightChild(), key);
        }
    }

    // TODO: Implement additional methods here (if needed).

    /**
     * Returns the tree node containing the maximum element, or null if this tree is
     * empty. (1 Punkt)
     * 
     * @return the tree node containing the maximum
     */
    public TreeNode maximum() {
        // TODO: Implement the algorithm here.
        /* we use the fact, that the max is the rightmost node */
        TreeNode x = this.root;
        while (x.getRightChild() != null) {
            x = x.getRightChild();
        }
        return x;
    }

    // TODO: Implement additional methods here (if needed).

    /**
     * Returns the tree node containing the minimum element, or null if this tree is
     * empty. (1 Punkt)
     * 
     * @return the tree node containing the minimum
     */
    public TreeNode minimum() {
        // TODO: Implement the algorithm here.
        /* uses the fact, that the minimum is the leftmost node */
        return this.minimum(this.root);
    }

    private TreeNode minimum(TreeNode node) {
        TreeNode x = node;
        while (x.getLeftChild() != null) {
            x = x.getLeftChild();
        }
        return x;
    }

    // TODO: Implement additional methods here (if needed).

    /**
     * Deletes and returns a values from the binary search tree. (5 Punkte)
     * 
     * @return the value to be returned.
     */
    public TreeNode delete(int value) {
        // TODO: Implement the algorithm here.
        TreeNode x;
        TreeNode y;
        /* as the method should delete a key, we first search the node */
        TreeNode z = this.search(value);
        /* we only need to delete if exists */
        if (z != null) {
            if (z.getLeftChild() == null || z.getRightChild() == null) {
                /* easy case, just need to splice out z */
                y = z;
            } else {
                /* hard case, 2 childs => need to find "Nachfolger" to replace Node */
                y = this.findSuccessor(z);
            }
            /* prepare to splice out */
            if (y.getLeftChild() != null) {
                x = y.getLeftChild();
            } else {
                x = y.getRightChild();
            }
            /* if y has no child, x is null */
            if (x != null) {
                x.setParentNode(y.getParent());
            }
            /* y could be the root, else adjust pointer accordingly */
            if (y.getParent() == null) {
                this.root = x;
            } else if (y.getParent().getLeftChild() != null && y.getValue() == y.getParent().getLeftChild().getValue()) {
                y.getParent().setLeftChild(x);
            } else {
                y.getParent().setRightChild(x);
            }
            if (y.getValue() != z.getValue()) {
                z.setValue(y.getValue());
            }
            /* return the removed node on success */
            return y;
        }
        return null;

    }

    /**
     * Finds the successor in the subtree given a Node
     * @param node the node whos successor is searched
     * @return the successor of node
     */
    private TreeNode findSuccessor(TreeNode node) {

        /* if we have a right subtree the successor is the minimum of the right subtree */
        if (node.getRightChild() != null) {
            return this.minimum(node.getRightChild());
        }
        TreeNode parent = node.getParent();
        /* otherwise the successor is the lowest anchestor of x, whose left child is also 
            an anchestor of x */
        while (parent != null && (node = parent.getRightChild()) != null) {
            node = parent;
            parent = parent.getParent();
        }
        return parent;
    }

    // TODO: Implement additional methods here (if needed).

    /**
     * Prints the elements of this binary search tree in an inorder traversal.
     */
    public void inorderTreeTraversal() {
        this.inorderTreeTraversal(this.root);
        System.out.println();
    }

    private void inorderTreeTraversal(TreeNode x) {
        if (x != null) {
            this.inorderTreeTraversal(x.getLeftChild());
            System.out.print(x.getValue() + " ");
            this.inorderTreeTraversal(x.getRightChild());
        }
    }
}