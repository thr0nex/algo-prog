package algo05;

/**
 * Implementation of BSTreeNode 
 * The getter and setter methods implemented here are not
 * commented in detail
 * @author yannickstreicher, benjaminrebholz, silviahimmelseher
 */
public class BSTreeNode implements TreeNode {

    // TODO: put class variables here
    // shouldnt this be key?
    /* no Data was needed to store */
    private int value;
    private TreeNode parent;
    private TreeNode left;
    private TreeNode right;

    public BSTreeNode(int value) {
        // TODO: Implement me
        /* default to empty node */
        this.value = value;
        this.parent = null;
        this.left = null;
        this.right = null;
    }

    public int getValue() {
        return this.value;
    }

    public TreeNode getLeftChild() {
        return this.left;
    }

    public TreeNode getRightChild() {
        return this.right;
    }

    public TreeNode getParent() {
        return this.parent;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setLeftChild(TreeNode v) {
        this.left = v;
    }

    public void setRightChild(TreeNode v) {
        this.right = v;
    }

    public void setParentNode(TreeNode v) {
        this.parent = v;
    }
    // TODO: Implement interface methods

}