package algo08;

import java.util.ArrayList;

/** 
 * Board Klasse von
 * @author Yannick Streicher, Silvia Himmelseher, Benjamin Rebholz
 */
public class Game {

	Board currentBoard;
	ArrayList<Integer> hashSequence; // contains the hash values of the game history

	/**
	 * Creates a new Game.
	 */
	Game() {
		this.currentBoard = new Board();
		this.hashSequence = new ArrayList<Integer>();
		this.hashSequence.add(currentBoard.hash);
	}

	/**
	 * Change one Square.
	 * 
	 * @param position position of the square
	 * @param newState new state of the square
	 */
	public synchronized void change(int position, int newState) {
		int hash = currentBoard.change(position, newState);
		if (hash != hashSequence.get(hashSequence.size() - 1)) {
			hashSequence.add(hash);
		}
	}

	/**
	 * to String
	 * 
	 * @return
	 */
	public String draw() {
		return this.currentBoard.draw();
	}

	/**
	 * Undo the last change in the game state
	 */
	public synchronized void undoLastChange() {

		// TODO: IMPLEMENT ME
		int stableHash = this.hashSequence.get(this.hashSequence.size() - 1);
		int prevState = this.hashSequence.get(this.hashSequence.size() - 2);

		int distance = stableHash ^ prevState;
		Square searchedSquare = null;

		/*
		 * distance = previousContent ^ lastChangesContent. lastChangesContent is still
		 * fields content
		 */
		for (Square sq : this.currentBoard.board) {
			int needle = distance ^ sq.getHash();
			if (sq.getHashEmpty() == needle) {
				sq.setContent(Square.EMPTY);
				searchedSquare = sq;
				break;
			} else if (sq.getHashBlack() == needle) {
				sq.setContent(Square.BLACK);
				searchedSquare = sq;
				break;
			} else if (sq.getHashWhite() == needle) {
				sq.setContent(Square.WHITE);
				searchedSquare = sq;
				break;
			}
		}
		if (searchedSquare == null) {
			throw new Error("shit amk");
		} else {
			this.hashSequence.remove((Object) stableHash);
			//this.hashSequence.remove((Object) prevState);
		}
	}
}
