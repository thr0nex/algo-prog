package algo;

import java.util.Arrays;

/**
 * Class for testing purposes of different searching algorithms
 * @author yannickstreicher, benjaminrebholz, silviahimmelseher
 */
public class Main {

	/* START OF TESTING */
	public static int[] emptyArray = {}; // empty
	public static int[] singletonArray = { 1 }; // = (1)
	public static int[] generalArray = new int[333];

	public static void setUpGeneralArray() {
		for (int i = 0; i < generalArray.length; i++) {
			generalArray[i] = 3 * i;
		}
	}

	public static void searchInArray(int[] array, int schluessel) {
		int linearResult = lineareSuche(array, schluessel);
		int binaryResult = binaereSuche(array, schluessel);
		int interpolResult = interpolationsSuche(array, schluessel);
		

		if (linearResult != -1) {
			System.out.println("Linear search: " + schluessel + " found in possition " + linearResult);
		} else {
			System.out.println("Linear search: " + schluessel + " not found in array");
		}

		if (binaryResult != -1) {
			System.out.println("Binary search: " + schluessel + " found in possition " + binaryResult);
		} else {
			System.out.println("Binary search: " + schluessel + " not found in array");
		}

		if (interpolResult != -1) {
			System.out.println("Interpolation search: " + schluessel + " found in possition " + interpolResult);
		} else {
			System.out.println("Interpolation search: " + schluessel + " not found in array");
		}

		System.out.println();
	}

	public static void main(String[] args) {
		setUpGeneralArray();

		// the keys to search
		int[] keys = { -10000, -1000, -333, -100, -2, -1, 0, 1, 2, 100, 333, 1000, 10000 };

		// test all keys
		for (int i = 0; i < keys.length; i++) {
			System.out.println("Searching for key " + keys[i] + " in the first array:");
			searchInArray(emptyArray, keys[i]);
			System.out.println("Searching for key " + keys[i] + " in the second array:");
			searchInArray(singletonArray, keys[i]);
			System.out.println("Searching for key " + keys[i] + " in the third array:");
			searchInArray(generalArray, keys[i]);
			System.out.println();
		}
	}
	/* END OF TESTING */

	/**
	 * Implementation of linear search. Return the index of <code>schluessel</code>
	 * in the array. Return -1 if <code>schluessel</code> is not contained in the
	 * array.
	 * 
	 * @param array
	 * @param schluessel
	 * @return
	 */
	public static int lineareSuche(int[] array, int schluessel) {
		/* index */
		int next = 0;

		/* check if array is well defined */
		if (array != null && array.length > 0) {
			for (int i = 0; i < array.length; i++) {
				if (array[i] == schluessel)  return i ;
			}
		}
		return -1;
	}

	/**
	 * Implementation of binary search. Return the index of <code>schluessel</code>
	 * in the array. Return -1 if <code>schluessel</code> is not contained in the
	 * array.
	 * 
	 * @param array
	 * @param schluessel
	 * @return
	 */
	public static int binaereSuche(int[] array, int schluessel) {
		int left = 0;
		int right = array.length - 1;
		/* check if array is well defined */
		if (array != null && array.length > 0) {
			while (left <= right) {
				/* calculation from the lecture */
				int middle = ((int) Math.ceil((right - left) / 2)) + left;
				if (schluessel == array[middle]) {
					return middle;
				} else if (schluessel < array[middle]) {
					right = middle - 1;
				} else {
					left = middle + 1;
				}
			}
		}
		return -1;
	}

	/**
	 * Implementation of interpolation search. Return the index of
	 * <code>schluessel</code> in the array. Return -1 if <code>schluessel</code> is
	 * not contained in the array.
	 * 
	 * @param array
	 * @param schluessel
	 * @return
	 */
	public static int interpolationsSuche(int[] array, int schluessel) {
		int left = 0;
		int right = array.length - 1;
		/* well defined? */
		if (array != null && array.length > 0) {
			/* additionally check if the calculated middle is out of bounds */
			while ((array[right] != array[left]) && (schluessel >= array[left]) && (schluessel <= array[right])) {

				/* calculation from the lecture */
				int distance = right - left;
				/* "Gauss Klammer" with floor div */
				int weight = Math.floorDiv((schluessel - array[left]), array[right] - array[left]);
				int middle = weight * distance + left;

				/* this equals binary search */
				if (schluessel == array[middle]) {
					return middle;
				} else if (schluessel < array[middle]) {
					right = middle - 1;
				} else {
					left = middle + 1;
				}
			}
		}
		return -1;
	}
}
