package algo03;

import java.util.Arrays;

/**
 * Implements timings of different sorting algorithms
 */
public class Main {
	// TODO: Set this to 'true', if you test your code and to 'false', if you want
	// the running time.
	private static final boolean TESTING = false;

	private static final int TEST_RUNS = 1; // for testing do a single run
	private static final int RUNNING_TIME_RUNS = 20; // to get the average running time use more runs
	private static final double MAX_ENTRY_PERCENTAGE = 0.25; // maximal entry in the array

	/* START OF TESTING */
	public static void main(String[] args) {
		java.util.Random random = new java.util.Random();

		java.util.Map<Integer, Long> heapSortRuntimeAverage = new java.util.TreeMap<Integer, Long>();
		java.util.Map<Integer, Long> quickSortRuntimeAverage = new java.util.TreeMap<Integer, Long>();
		java.util.Map<Integer, Long> countingSortRuntimeAverage = new java.util.TreeMap<Integer, Long>();

		int RUNS = TESTING ? TEST_RUNS : RUNNING_TIME_RUNS;
		if (!TESTING) {
			System.out.println("Doing " + RUNS + " runs of each algorithm. This can take some time.");
			System.out.print("Current array size: ");
		}

		for (int arraySize = 100000; arraySize <= 1000000; arraySize = arraySize + 100000) {

			if (!TESTING) {
				System.out.print(arraySize + "...");
			}

			long runtimeHeapSort = 0;
			long runtimeQuicksort = 0;
			long runtimeCountingsort = 0;

			for (int run = 1; run <= RUNS; run++) {

				// Create a new array with integer values and fill it with random numbers.
				int[] randomArray = new int[arraySize];
				for (int i = 0; i < randomArray.length; i++) {
					randomArray[i] = random.nextInt((int) (arraySize * MAX_ENTRY_PERCENTAGE));
				}

				// Sort with heapSort
				int[] copy1 = java.util.Arrays.copyOf(randomArray, randomArray.length);
				long timerStart = System.currentTimeMillis();
				heapSort(copy1);
				long runtime = System.currentTimeMillis() - timerStart;
				runtimeHeapSort += runtime;

				if (TESTING) {
					for (int i = 0; i < copy1.length - 1; i++) {
						if (copy1[i] > copy1[i + 1]) {
							System.err.println("Error in heapSort at position " + i);
							break;
						}
					}
				}

				// Sort with quickSort
				int[] copy2 = java.util.Arrays.copyOf(randomArray, randomArray.length);
				timerStart = System.currentTimeMillis();
				quickSort(copy2);
				runtime = System.currentTimeMillis() - timerStart;
				runtimeQuicksort += runtime;

				if (TESTING) {
					for (int i = 0; i < copy2.length - 1; i++) {
						if (copy2[i] > copy2[i + 1]) {
							System.err.println("Error in quickSort at position " + i);
							break;
						}
					}
				}

				// Sort with countingSort
				int[] copy3 = java.util.Arrays.copyOf(randomArray, randomArray.length);
				timerStart = System.currentTimeMillis();
				countingSort(copy3);
				runtime = System.currentTimeMillis() - timerStart;
				runtimeCountingsort += runtime;

				if (TESTING) {
					for (int i = 0; i < copy2.length - 1; i++) {
						if (copy3[i] > copy3[i + 1]) {
							System.err.println("Error in countingSort at position " + i);
							break;
						}
					}
				}
			}

			heapSortRuntimeAverage.put(arraySize, runtimeHeapSort / RUNS);
			quickSortRuntimeAverage.put(arraySize, runtimeQuicksort / RUNS);
			countingSortRuntimeAverage.put(arraySize, runtimeCountingsort / RUNS);
		}

		if (TESTING) {
			System.out.println("If no errors have been printed, then you passed the test ");
		} else {
			// print the running time

			System.out.println();
			System.out.println();

			System.out.println("Heap Sort");
			heapSortRuntimeAverage.forEach((key, value) -> System.out.println(key + ": " + value));

			System.out.println();

			System.out.println("Quick Sort");
			quickSortRuntimeAverage.forEach((key, value) -> System.out.println(key + ": " + value));

			System.out.println();

			System.out.println("Counting Sort");
			countingSortRuntimeAverage.forEach((key, value) -> System.out.println(key + ": " + value));

		}
	}
	/* END OF TESTING */

	/* START OF HEAPSORT */

	/**
	 * Calls heapSort.
	 * 
	 * @param array
	 */
	public static void heapSort(int[] array) {

		// TODO: Implement heapsort here
		int heapSize = array.length;
		buildMaxHeap(array);
		for (int i = heapSize - 1; i >= 0; i--) {
			swap(array, 0, i);
			maxHeapify(array, 0, i);
		}
	}

	// TODO: Put additional methods for heapsort here

	/**
	 * Basic swap
	 * 
	 * @param arr the referenced array
	 * @param i
	 * @param j
	 */
	private static void swap(int[] arr, int i, int j) {
		int tmp = arr[j];
		arr[j] = arr[i];
		arr[i] = tmp;
	}

	/**
	 * Initializes the max - heap, the top element in the heap is the maximum
	 * 
	 * @param arr the array in which the heap is stored
	 */
	private static void buildMaxHeap(int[] arr) {
		for (int i = (arr.length / 2) - 1; i >= 0; i--) {
			maxHeapify(arr, i, arr.length);
		}
	}

	/**
	 * implements heapify / sink
	 */
	private static void maxHeapify(int[] arr, int i, int heapSize) {

		int largest = i;
		int l = 2 * i + 1;
		int r = 2 * i + 2;

		if (l < heapSize && arr[l] > arr[largest])
			largest = l;
		if (r < heapSize && arr[r] > arr[largest])
			largest = r;
		if (largest != i) {
			int tmp = arr[largest];
			arr[largest] = arr[i];
			arr[i] = tmp;
			maxHeapify(arr, largest, heapSize);
		}
	}


	/* END OF HEAPSORT */

	/* START OF QUICKSORT */

	/**
	 * Sorts an array by the divide-and-conquer approach with quicksort.
	 * 
	 * @param array
	 */
	public static void quickSort(int[] array) {

		// TODO: Implement quicksort here
		startQuicksort(array, 0, array.length - 1);
	}

	// TODO: Put additional methods for quicksort here
	/**
	 * To realise the recursion we need this start method
	 * @param arr
	 * @param p
	 * @param r
	 */
	public static void startQuicksort(int[] arr, int p, int r) {
		if (p < r) {
			/* get the pivot position */
			int q = partition(arr, p, r);
			startQuicksort(arr, p, q - 1);
			startQuicksort(arr, q + 1, r);
		}
	}

	/**
	 * returns the pivots position
	 * @param arr
	 * @param p
	 * @param r
	 * @return
	 */
	public static int partition(int[] arr, int p, int r) {
		/* pivot */
		int x = arr[r];
		int i = p - 1;
		for (int j = p; j <= r - 1; j++) {
			/* between p and i are values <= pivot x */
			if (arr[j] <= x) {
				i++;
				/* all values between i + 1 and j are > pivot x */
				swap(arr, i, j);
			}
		}

		/* position the pivot */
		swap(arr, i + 1, r);
		return i + 1;
	}

	/* END OF QUICKSORT */

	/* START OF COUNTINGSORT */

	/**
	 * Sorts an array of integers.
	 * 
	 * @param array
	 */
	public static void countingSort(int[] array) {

		// TODO: Implement countingsort here
		int[] brr = Arrays.copyOf(array, array.length);
		startCountingsort(array, brr, array.length);

	}

	// TODO: Put additional methods for countingsort here
	/**
	 * Implements the counting sort procedere
	 * @param arr the source array
	 * @param brr a temporal buffer
	 * @param k the maximum number possible
	 */
	public static void startCountingsort(int[] arr, int[] brr, int k) {
		
		/* we have to take a large enough counting array. As we did not want to generate
			additional runtime, we did use a big numer */
		int[] counts = new int[(int) Math.pow(10, 7)];
		for (int i = 0; i < counts.length; i++) {
			counts[i] = 0;
		}
		for (int j = 0; j < arr.length; j++) {
			counts[arr[j]]++;
		}
		/* counts[i] now contains number of equal elements to i */
		for (int i = 1; i < counts.length; i++) {
			counts[i] = counts[i] + counts[i - 1];
		}
		/* counts[i] now contains number of elements <= i */
		for (int j = arr.length - 1; j >= 0; j--) {
			brr[counts[arr[j]] - 1] = arr[j];
			counts[arr[j]] = counts[arr[j]] - 1;
		}
		/* brr is now sorted */
		for (int i = 0; i < brr.length; i++) {
			arr[i] = brr[i];
		}
		/* arr is now sorted */
	}

	/* END OF COUNTINGSORT */
}