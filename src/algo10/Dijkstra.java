package algo10;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Comparator;

/**
 * Djikstra implementation by
 * @author yannickstreicher, silviahimmelseher, benjamin 
 */
public class Dijkstra {

	/**
	 * executes our implementation of djikstra
	 * @param g
	 * @param start
	 */
	public static void runDijkstra(Graph g, Node start) {
		// init shortest path
		start.setDistance(0);

		for (Node node : g.nodes()) {
			if (node.getDistance() == -1) {
				node.setDistance(Integer.MAX_VALUE);
			}
		}

		// nodes of shortest path
		HashSet<Node> s = new HashSet<Node>();

		// the nodes are stored on min heap
		PriorityQueue<Node> minQ = new PriorityQueue<Node>(new Comparator<Node>() {
			@Override
			public int compare(Node x, Node q) {			
				return x.getDistance() - q.getDistance();
			}
		});

		minQ.addAll(g.nodes());

		while (!minQ.isEmpty()) {
			Node u = minQ.poll();
			s.add(u);
			for (Edge e : u.getAdjacentEdges()) {
				relax(e, minQ);
			}
		}
	}

	/**
	 * relax function, oriented on clrs it updates the distance entries of vertices,
	 * if there is a better way to get there
	 */
	private static void relax(Edge e, PriorityQueue<Node> minQ) {
		Node source = e.getSource();
		Node target = e.getTarget();
		if (target.getDistance() > source.getDistance() + e.getCost()) {
			target.setDistance(source.getDistance() + e.getCost());
			target.setPredecessor(source);
			// this is needed to force a heapify()
			minQ.remove((Object) target);
			minQ.offer(target);
		}
	}

	/**
	 * print the shortest path in djikstra solution
	 * 
	 * @param g
	 * @param t
	 */
	private static void printShortestPath(Graph g, Node t) {
		Node tmpt = t;
		StringBuilder st = new StringBuilder();
		while (tmpt.getPredecessor() != null) {
			st.insert(0, " -> " + tmpt);
			tmpt = tmpt.getPredecessor();
		}
		st.insert(0, " -> " + tmpt);
		System.out.println(st);
	}

	public static void main(String[] args) {
		// This is the graph from Exercise 4 of P� 9.
		Graph g = new Graph();

		Node s = g.createNode();
		Node a = g.createNode();
		Node b = g.createNode();
		Node c = g.createNode();
		Node d = g.createNode();
		Node e = g.createNode();
		Node f = g.createNode();
		Node t = g.createNode();

		g.createEdge(s, a, 4);
		g.createEdge(s, b, 6);
		g.createEdge(a, c, 6);
		g.createEdge(b, c, 2);
		g.createEdge(b, d, 5);
		g.createEdge(c, d, 2);
		g.createEdge(c, e, 7);
		g.createEdge(d, e, 5);
		g.createEdge(d, f, 1);
		g.createEdge(f, e, 3);
		g.createEdge(e, t, 5);
		g.createEdge(f, t, 9);

		runDijkstra(g, s);
		g.nodes().stream().forEach(u -> {
			System.out.println(u + " " + u.getDistance());
		});
		printShortestPath(g, t);
	}
}
